from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

import json
import logging
from typing import Text, Dict, Any, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction
from rasa_sdk.events import SlotSet, UserUtteranceReverted, \
	ConversationPaused, FollowupAction, Form
from rasa_sdk.events import SlotSet, Form
from lib.get_responses_from_db import fetch_faq_response_api, format_api_response, add_extra_params_for_analytics
from lib import slotStorage

logger = logging.getLogger(__name__)

class ActionDefaultAskAffirmation(Action):
	"""Asks for an affirmation of the intent if NLU threshold is not met."""

	def name(self) -> Text:
		return "action_default_ask_affirmation"

	def run(self,
			dispatcher: CollectingDispatcher,
			tracker: Tracker,
			domain: Dict[Text, Any]
			) -> List['Event']:

		# try:
			intent_name = tracker.latest_message["intent"]["name"]
			sender_id = tracker.sender_id
			group_id = slotStorage.get_slot(sender_id,"group_id")
			platform = slotStorage.get_slot(sender_id,"platform")
			ssml = "<s>I am sorry! I am still learning and I could not understand what you said. Could you please say that again?</s>"
			fallback_message = [{ "message": { "template": { "elements": { "title": "I am sorry! I am still learning and I could not understand what you said. Could you please say that again?", "says": "", "visemes": ""},"type": "Card"} } },{"ssml": "<speak><s>I am sorry! I am still learning and I could not understand what you said. Could you please say that again?</s></speak>"}]
			formatted_response = add_extra_params_for_analytics(fallback_message,_o_steps=99,usecase="UBAR268",request_id=slotStorage.get_slot(sender_id,"message_id"),tag=tracker.latest_message["text"],m_id=slotStorage.get_slot(sender_id,"message_id"),reply="N")
			dispatcher.utter_message(json.dumps(fallback_message))
		# except:
		#     print("Could'nt enter the missed utterance")
		# return []